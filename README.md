# EpidemicDS

#### 介绍
一个简单的基于Java的Springboot框架构建的疫情数据统计系统。

#### 软件技术栈
JDK:1.8
jquery:3.3.1
lombok
后端：Springboot2.7.4+安全框架:springboot-security+mybatis2.1.1
数据库：Mysql8.0.12+Druid:1.0.6
前端：HTML5+thymeleaf模板
分页插件：pagehelper1.2.5




#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



