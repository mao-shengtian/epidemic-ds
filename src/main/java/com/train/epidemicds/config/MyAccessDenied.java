package com.train.epidemicds.config;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyAccessDenied implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //设置相应状态码
        response.setStatus(response.SC_FORBIDDEN);
        //设置相应数据格式
        response.setContentType("application/json;charset=utf-8");
        //设置响应内容
        response.sendRedirect("/noAccess");
    }
}
