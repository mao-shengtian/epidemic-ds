package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Base;
import com.train.epidemicds.entity.Patient;
import org.apache.ibatis.annotations.*;

import java.sql.Date;
import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/13
 */


@Mapper
public interface PatientMapper {
    @Select("SELECT baseId,symptoms,hospital,critical,note,infectionSource,onsetDate FROM patient  where baseId NOT IN(SELECT baseId FROM dead) AND baseId NOT IN(SELECT baseId FROM cure)")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "symptoms", column = "symptoms"),
            @Result(property = "hospital", column = "hospital"),
            @Result(property = "critical", column = "critical"),
            @Result(property = "infectionSource", column = "infectionSource"),
            @Result(property = "onsetDate", column = "onsetDate"),
            @Result(property = "note", column = "note"),
            @Result(property = "inspects", column = "baseId", javaType = java.util.List.class, many = @Many(select = "com.train.epidemicds.mapper.InspectMapper.findById")),
            @Result(property = "base", column = "baseId", javaType = Base.class, one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    List<Patient> findAll();

    @Insert("insert into patient(baseId,symptoms,hospital,critical,infectionSource,onsetDate,note)values(#{baseId},#{symptoms},#{hospital},#{critical},#{infectionSource},#{onsetDate},#{note})")
    void add(Patient patient);

    @Update("update patient set symptoms=#{symptoms},hospital=#{hospital},critical=#{critical},infectionSource=#{infectionSource},onsetDate=#{onsetDate},note=#{note} where baseId=#{baseId}")

    void update(Patient patient);

    @Select("SELECT baseId,symptoms,hospital,critical,note,infectionSource,onsetDate " +
            " FROM patient WHERE  baseId NOT IN(SELECT baseId FROM dead) AND baseId NOT IN(SELECT baseId FROM cure) and baseId in(select id from base where name like #{name})")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "symptoms", column = "symptoms"),
            @Result(property = "hospital", column = "hospital"),
            @Result(property = "critical", column = "critical"),
            @Result(property = "infectionSource", column = "infectionSource"),
            @Result(property = "onsetDate", column = "onsetDate"),
            @Result(property = "note", column = "note"),
            @Result(property = "inspects", column = "baseId", javaType = java.util.List.class, many = @Many(select = "com.train.epidemicds.mapper.InspectMapper.findById")),
            @Result(property = "base", column = "baseId", javaType = Base.class, one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    List<Patient> findByName(String name);

    @Select("select baseId,symptoms,hospital,critical,note,infectionSource,onsetDate from patient where baseId=#{baseId}")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "symptoms", column = "symptoms"),
            @Result(property = "hospital", column = "hospital"),
            @Result(property = "critical", column = "critical"),
            @Result(property = "infectionSource", column = "infectionSource"),
            @Result(property = "onsetDate", column = "onsetDate"),
            @Result(property = "note", column = "note"),
            @Result(property = "inspects", column = "baseId", javaType = java.util.List.class, many = @Many(select = "com.train.epidemicds.mapper.InspectMapper.findById")),
            @Result(property = "base", column = "baseId", javaType = Base.class, one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    Patient findById(int baseId);

    @Select("select sum(1) from patient")
    int number();

    @Select("select sum(1) from patient where baseId not in(select baseId from cure) and baseId not in(select baseId from dead)")
    int currentNumber();

    @Select("select sum(1) from patient where onsetDate <#{date}")
    int beforeDay(Date date);
}
