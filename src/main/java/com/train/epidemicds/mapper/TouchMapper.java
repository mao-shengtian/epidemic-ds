package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Base;
import com.train.epidemicds.entity.Touch;
import org.apache.ibatis.annotations.*;

import java.util.List;
/**
 * author:刘哲楷
 * date:2022/10/13
 */
@Mapper
public interface TouchMapper {
    @Select("select baseId,comeFrom,isoAddress,startDate,finished from touch where finished ='否' ")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "comeFrom", column = "comeFrom"),
            @Result(property = "isoAddress", column = "isoAddress"),
            @Result(property = "startDate", column = "startDate"),
            @Result(property = "finished", column = "finished"),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    List<Touch> findAll();

    @Insert("insert into touch(baseId,comeFrom,isoAddress,startDate,finished)values(#{baseId},#{comeFrom},#{isoAddress},#{startDate},#{finished})")
    void add(Touch touch);

    @Select("select baseId,comeFrom,isoAddress,startDate,finished from touch where baseId =#{baseId} ")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "comeFrom", column = "comeFrom"),
            @Result(property = "isoAddress", column = "isoAddress"),
            @Result(property = "startDate", column = "startDate"),
            @Result(property = "finished", column = "finished"),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    Touch findById(int baseId);

    @Select("SELECT baseId,comeFrom,isoAddress,startDate,finished " +
            " FROM touch WHERE baseId in(select id from base where name like #{name})")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "comeFrom", column = "comeFrom"),
            @Result(property = "isoAddress", column = "isoAddress"),
            @Result(property = "startDate", column = "startDate"),
            @Result(property = "finished", column = "finished"),
            @Result(property = "base", column = "baseId", javaType = Base.class, one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    List<Touch> findByName(String name);

    @Update("update touch set finished='是' where baseId=#{baseId}")
    void toSafe(int baseId);

    @Select("select sum(1) from touch where finished ='否' ")
    Integer currentNumber();

    @Select("select sum(1) from touch  ")
    Integer number();
}
