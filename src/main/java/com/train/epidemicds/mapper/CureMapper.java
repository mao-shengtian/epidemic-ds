package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Base;
import com.train.epidemicds.entity.Cure;
import com.train.epidemicds.entity.Patient;
import org.apache.ibatis.annotations.*;

import java.sql.Date;
import java.util.List;
import java.util.Map;
/**
 * author:刘炫
 * date:10/13
 */

@Mapper
public interface CureMapper {
    @Select("select baseId,dischargeDate, current from cure")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "patient",column = "baseId",javaType = Patient.class,one = @One(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "dischargeDate", column = "dischargeDate"),
            @Result(property = "current", column = "current"),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))

    })
    //查看所有治愈病人
    public List<Cure> findAll();
    @Insert("insert into Cure(baseId,dischargeDate,current)values(#{baseId},#{dischargeDate},#{current})")
    //添加治愈病人
    public void add(Cure cure);
    //根据id查找治愈病人
    @Select("select baseId,dischargeDate, current from cure where baseId = #{id}")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "dischargeDate", column = "dischargeDate"),
            @Result(property = "current", column = "current"),
            @Result(property = "patient",column = "baseId",javaType = Patient.class,one = @One(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    //根据治愈Id查找治愈病人
    public Cure findById(int id);
    @Update("update  cure set current=#{current} where baseId=#{baseId}")
    //修改治愈病人数据
    public void update(int baseId,String current);
    @Select("select sum(1) from cure")
    public int number();
    @Select(" SELECT SUM(1),dischargeDate  FROM cure GROUP BY dischargeDate;")
    public List<Map<Integer, Date>> group();
    @Select("select sum(1) from cure where dischargeDate <#{date}")
    public int beforeDay(Date date);
    @Select("select baseId,dischargeDate, current from cure WHERE baseId in(select id from base where name like #{name})")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "patient",column = "baseId",javaType = Patient.class,many = @Many(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "dischargeDate", column = "dischargeDate"),
            @Result(property = "current", column = "current"),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))

    })
    public List<Cure> findByName(String name);

}
