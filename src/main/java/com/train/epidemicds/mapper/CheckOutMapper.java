package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.CheckOut;
import org.apache.ibatis.annotations.*;

import java.util.List;
/**
 * author:刘炫
 * date:10/13
 */

@Mapper
public interface CheckOutMapper {
    @Insert("insert into checkout(baseId,currentPosition,bodyTemperature,healthState,isToHighArea,isTouch,remarks,name,checkOutDate)values(#{baseId},#{currentPosition},#{bodyTemperature},#{healthState},#{isToHighArea},#{isTouch},#{remarks},#{name},#{checkOutDate})")
    public void add(CheckOut checkOut);

    @Select("select currentPosition,bodyTemperature,healthState,isToHighArea,isTouch,remarks,name,checkOutDate from checkout ")
    @Results({
            @Result(property = "currentPosition", column = "currentPosition"),
            @Result(property = "bodyTemperature", column = "bodyTemperature"),
            @Result(property = "healthState", column = "healthState"),
            @Result(property = "isToHighArea", column = "isToHighArea"),
            @Result(property = "isTouch", column = "isTouch"),
            @Result(property = "remarks", column = "remarks"),
            @Result(property = "name", column = "name"),
            @Result(property = "checkOutDate", column = "checkOutDate"),
    })
    public List<CheckOut> findAll();

    //查找
    @Select("select * from checkout where name like #{name}")
    @Results({
            @Result(property = "currentPosition", column = "currentPosition"),
            @Result(property = "bodyTemperature", column = "bodyTemperature"),
            @Result(property = "healthState", column = "healthState"),
            @Result(property = "isToHighArea", column = "isToHighArea"),
            @Result(property = "isTouch", column = "isTouch"),
            @Result(property = "remarks", column = "remarks"),
            @Result(property = "name", column = "name"),
            @Result(property = "checkOutDate", column = "checkOutDate"),
    })

    public List<CheckOut> findByName(String name);

}
