package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Inspect;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author:刘炫
 * date:10/13
 */

@Mapper
public interface InspectMapper {
    @Select("select testId ,baseId,testDate,ctTest,nuTest from inspect where baseId=#{baseId}")
    public List<Inspect> findById(int baseId);

    @Insert("insert into inspect(baseId,testDate,ctTest,nuTest)values(#{baseId},#{testDate},#{ctTest},#{nuTest})")
    public void add(Inspect inspect);
}
