package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Source;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/13
 */

@Mapper
public interface SourceMapper {
    @Select("select id ,name,type,number,hospitalId from source where hospitalId=#{hospitalId} ")
    List<Source> findByHospitalId(int hospitalId);

    @Delete("delete from source where id=#{id}")
    void deleteById(int id);

    @Insert("insert into source(name,type,number,hospitalId) values(#{name},#{type},#{number},#{hospitalId})")
    void addSource(Source source);

    @Update("update source set name=#{name},type=#{type},number=#{number} where id=#{id}")
    void update(Source source);

}
