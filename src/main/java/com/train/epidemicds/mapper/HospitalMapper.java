package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Hospital;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
/**
 * author:刘炫
 * date:10/13
 */

@Mapper
public interface HospitalMapper {
    @Select("select id,name,address,phone from hospital")
    public List<Hospital> findAll();

    @Insert("insert into hospital(name,address,phone)values(#{name},#{address},#{phone})")
    public boolean add(Hospital hospital);

    @Select("select id,name,address,phone from hospital where name like #{name}")
    public List<Hospital> findByName(String name);

    @Update("update hospital set name=#{name},address=#{address},phone=#{phone} where id=#{id}")
    public boolean update(Hospital hospital);
}
