package com.train.epidemicds.mapper;

import com.train.epidemicds.entity.Base;
import com.train.epidemicds.entity.Dead;
import com.train.epidemicds.entity.Patient;
import org.apache.ibatis.annotations.*;

import java.util.List;
/**
 * author:刘炫
 * date:10/13
 */

@Mapper
public interface DeadMapper {
    @Select("select baseId,deadTime from dead")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "deadTime", column = "deadTime"),
            @Result(property = "patient",column = "baseId",javaType = Patient.class,one = @One(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    public List<Dead> findAll();

    @Insert("insert into dead(baseId,deadTime)values(#{baseId},#{deadTime})")
    public void add(Dead dead);

    @Select("select baseId,deadTime from dead where baseId = #{baseId}")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "deadTime", column = "deadTime"),
            @Result(property = "patient",column = "baseId",javaType = Patient.class,one = @One(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "base",column = "baseId",javaType = Base.class,one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    public Dead findById(int baseId);

    @Select("select sum(1) from dead")
    public int number();

    @Select("SELECT baseId,deadTime " +
            " FROM dead WHERE baseId in(select id from base where name like #{name})")
    @Results({
            @Result(id = true, property = "baseId", column = "baseId"),
            @Result(property = "deadTime", column = "deadTime"),
            @Result(property = "patient", column = "baseId", javaType = Patient.class,many = @Many(select = "com.train.epidemicds.mapper.PatientMapper.findById")),
            @Result(property = "base", column = "baseId", javaType = Base.class, one = @One(select = "com.train.epidemicds.mapper.BaseMapper.findById"))
    })
    public List<Dead> findByName(String name);
}
