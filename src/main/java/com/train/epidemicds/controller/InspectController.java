package com.train.epidemicds.controller;

import com.train.epidemicds.entity.Inspect;
import com.train.epidemicds.service.InspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
/**
 * @Author: 小汪不爱起床
 * @Date: 2022/10/16
 */
@Controller
public class InspectController {
    @Autowired
    InspectService service;

    @RequestMapping(value = "/inspect/get/{id}",method = RequestMethod.GET)
    public List<Inspect> get(@PathVariable("id")int id){
        return service.find(id);
    }

    @RequestMapping(value="/inspect/add",method= RequestMethod.POST)
    public void add(@RequestBody Inspect Inspect)
    {
         service.add(Inspect);
    }

}
