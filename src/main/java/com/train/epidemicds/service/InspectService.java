package com.train.epidemicds.service;

import com.train.epidemicds.entity.Inspect;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */
public interface InspectService {
    public List<Inspect> find(int baseId);

    public void add(Inspect inspect);
}
