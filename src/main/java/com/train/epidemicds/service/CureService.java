package com.train.epidemicds.service;

import com.train.epidemicds.entity.Cure;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * author:刘炫
 * date:10/14
 */
public interface CureService {
    void add(Cure cure);
    List<Cure> findAll(int page, int size);
    Cure findById(int id);
    void update(int baseId,String current);
    int number();
    List<Map<Integer, Date>> group();
    int beforeDay(Date date);
    List<Cure> findByName(String name);
}
