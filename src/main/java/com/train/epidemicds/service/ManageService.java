package com.train.epidemicds.service;

import com.train.epidemicds.entity.Manage;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
public interface ManageService {
    List<Manage> findAll();


    public void add(Manage manage);

    public void delete(String id);

    public void update(Manage manage);

    public Manage find(String id,String password);
}
