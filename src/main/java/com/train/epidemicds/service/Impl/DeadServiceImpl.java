package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.Dead;
import com.train.epidemicds.mapper.DeadMapper;
import com.train.epidemicds.service.DeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author:刘炫
 * date:10/14
 */
@Service
public class DeadServiceImpl implements DeadService {
    @Autowired
    DeadMapper deadMapper;

    @Override
    public void add(Dead dead) {
        deadMapper.add(dead);
    }

    @Override
    public List<Dead> findAll(int page, int size) {
        PageHelper.startPage(page,size);
        return deadMapper.findAll();
    }

    @Override
    public Dead findById(int baseId) {
        return deadMapper.findById(baseId);
    }

    @Override
    public int number() {
        return deadMapper.number();
    }

    @Override
    public List<Dead> findByName(String name) {
        return deadMapper.findByName(name);
    }
}
