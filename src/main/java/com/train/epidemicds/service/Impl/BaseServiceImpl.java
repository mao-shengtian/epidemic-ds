package com.train.epidemicds.service.Impl;

import com.train.epidemicds.entity.Base;
import com.train.epidemicds.mapper.BaseMapper;
import com.train.epidemicds.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */

@Service
public class BaseServiceImpl implements BaseService {
    @Autowired
    BaseMapper baseMapper;

    @Override
    public Base findById(int id) {
        return baseMapper.findById(id);
    }

    @Override
    public void delete(int id) {
        baseMapper.delete(id);
    }

    @Override
    public void add(Base base) {
        baseMapper.add(base);
    }

    @Override
    public Base findByIdCard(String idCard) {
        return baseMapper.findByIdCard(idCard);
    }

    @Override
    public void update(Base base) {
        baseMapper.update(base);
    }

    @Override
    public List<Base> findAll(int page, int size) {
        return null;
    }
}
