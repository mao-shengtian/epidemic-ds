package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.Patient;
import com.train.epidemicds.mapper.PatientMapper;
import com.train.epidemicds.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
@Service
public class PatientServiceImpl implements PatientService {
    @Autowired
    PatientMapper patientMapper;

    @Override
    public List<Patient> findAll(int size, int page) {
        PageHelper.startPage(page,size);
        return patientMapper.findAll();
    }

    @Override
    public void add(Patient patient) {
        patientMapper.add(patient);
    }

    @Override
    public void update(Patient patient) {
        patientMapper.update(patient);
    }

    @Override
    public List<Patient> findByName(String name) {
        return patientMapper.findByName("%"+name+"%");
    }

    @Override
    public Patient findById(int id) {
        return patientMapper.findById(id);
    }

    @Override
    public int number() {
        return patientMapper.number();
    }

    @Override
    public int currentNumber() {
        return patientMapper.currentNumber();
    }

    @Override
    public int beforeDay(Date date) {
        return patientMapper.beforeDay(date);
    }
}
