package com.train.epidemicds.service.Impl;

import com.train.epidemicds.entity.Inspect;
import com.train.epidemicds.mapper.InspectMapper;
import com.train.epidemicds.service.InspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author:刘炫
 * date:10/14
 */
@Service
public class InspectServiceImpl implements InspectService {
    @Autowired
    InspectMapper inspectMapper;

    @Override
    public List<Inspect> find(int baseId) {
        return inspectMapper.findById(baseId);
    }

    @Override
    public void add(Inspect inspect) {
        inspectMapper.add(inspect);
    }
}
