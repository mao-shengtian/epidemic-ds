package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.Cure;
import com.train.epidemicds.mapper.CureMapper;
import com.train.epidemicds.service.CureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * author:刘炫
 * date:10/14
 */
@Service
public class CureServiceImpl implements CureService {
    @Autowired
    CureMapper cureMapper;

    @Override
    public void add(Cure cure) {
        cureMapper.add(cure);
    }

    @Override
    public List<Cure> findAll(int page, int size) {
        PageHelper.startPage(page, size);
        return cureMapper.findAll();
    }

    @Override
    public Cure findById(int id) {
        Cure cure = cureMapper.findById(id);
        return cure;
    }

    @Override
    public void update(int baseId, String current) {
        cureMapper.update(baseId, current);
    }

    @Override
    public int number() {
        return cureMapper.number();
    }

    @Override
    public List<Map<Integer, Date>> group() {
        return cureMapper.group();
    }

    @Override
    public int beforeDay(Date date) {
        return cureMapper.beforeDay(date);
    }

    @Override
    public List<Cure> findByName(String name) {
        return cureMapper.findByName(name);
    }
}
