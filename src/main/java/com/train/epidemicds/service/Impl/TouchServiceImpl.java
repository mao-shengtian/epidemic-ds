package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.Touch;
import com.train.epidemicds.mapper.TouchMapper;
import com.train.epidemicds.service.TouchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
@Service
public class TouchServiceImpl implements TouchService {
    @Autowired
    TouchMapper touchMapper;

    @Override
    public List<Touch> findAll(int page, int size) {
        PageHelper.startPage(page,size);
        return touchMapper.findAll();
    }

    @Override
    public void add(Touch touch) {
        touchMapper.add(touch);
    }

    @Override
    public List<Touch> findByName(String name) {
        return touchMapper.findByName("%"+name+"%");
    }

    @Override
    public Touch findById(int baseId) {
        return touchMapper.findById(baseId);
    }

    @Override
    public void toSafe(int baseId) {
        touchMapper.toSafe(baseId);
    }

    @Override
    public Integer number() {
        Integer count = touchMapper.number();
        if (count == null){
            return 0;
        }
        return count;
    }

    @Override
    public Integer currentNumber() {
        Integer count = touchMapper.currentNumber();
        if (count == null){
            return 0;
        }
        return count;
    }
}
