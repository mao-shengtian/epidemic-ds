package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.User;
import com.train.epidemicds.mapper.UserMapper;
import com.train.epidemicds.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
    }

    @Override
    public void deleteUser(int id) {
        userMapper.deleteUser(id);
    }

    @Override
    public List<User> findAll(int page, int size) {
        PageHelper.startPage(page,size);
        return userMapper.findAll();
    }

    @Override
    public void addUserRole(int id) {
        userMapper.addUserRole(id);
    }

    @Override
    public void addAdminRole(int id) {
        userMapper.addAdminRole(id);
    }
}
