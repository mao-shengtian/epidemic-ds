package com.train.epidemicds.service.Impl;

import com.train.epidemicds.entity.Authentication;
import com.train.epidemicds.mapper.AuthenticationMapper;
import com.train.epidemicds.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    AuthenticationMapper authenticationMapper;

    @Override
    public List<Authentication> findByUserId(int userId) {
        return authenticationMapper.findByUserId(userId);
    }
}
