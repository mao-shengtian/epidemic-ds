package com.train.epidemicds.service.Impl;

import com.github.pagehelper.PageHelper;
import com.train.epidemicds.entity.CheckOut;
import com.train.epidemicds.mapper.CheckOutMapper;
import com.train.epidemicds.service.CheckOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */
@Service
public class CheckOutServiceImpl implements CheckOutService {
    @Autowired
    public CheckOutMapper checkOutMapper;

    @Override
    public void add(CheckOut checkOut) {
        checkOutMapper.add(checkOut);
    }

    @Override
    public List<CheckOut> findAll(int page, int size) {
        PageHelper.startPage(page,size);
        return checkOutMapper.findAll();
    }

    @Override
    public List<CheckOut> findByName(String name) {
        return checkOutMapper.findByName(name);
    }
}
