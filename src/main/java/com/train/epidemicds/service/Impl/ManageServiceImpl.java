package com.train.epidemicds.service.Impl;

import com.train.epidemicds.entity.Manage;
import com.train.epidemicds.mapper.ManageMapper;
import com.train.epidemicds.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
@Service
public class ManageServiceImpl implements ManageService {
    @Autowired
    ManageMapper manageMapper;

    @Override
    public List<Manage> findAll() {
        return manageMapper.findAll();
    }

    @Override
    public void add(Manage manage) {
        manageMapper.add(manage);
    }

    @Override
    public void delete(String id) {
        manageMapper.delete(id);
    }

    @Override
    public void update(Manage manage) {
        manageMapper.update(manage);
    }

    @Override
    public Manage find(String id, String password) {
        return manageMapper.find(id,password);
    }
}
