package com.train.epidemicds.service;

import com.train.epidemicds.entity.Hospital;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */
public interface HospitalService {
    public List<Hospital> findAll();

    public boolean add(Hospital hospital);

    public List<Hospital> findByName(String name);

    public boolean update(Hospital hospital);
}
