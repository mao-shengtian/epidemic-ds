package com.train.epidemicds.service;

import com.train.epidemicds.entity.CheckOut;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */

public interface CheckOutService {
    void add(CheckOut checkOut);

    List<CheckOut> findAll(int page, int size);

    List<CheckOut> findByName(String name);
}