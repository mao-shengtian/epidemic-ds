package com.train.epidemicds.service;

import com.train.epidemicds.entity.Authentication;

import java.util.List;

public interface AuthenticationService {
    public List<Authentication> findByUserId(int userId);
}
