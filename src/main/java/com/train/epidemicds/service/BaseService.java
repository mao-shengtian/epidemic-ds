package com.train.epidemicds.service;

import com.train.epidemicds.entity.Base;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */

public interface BaseService {
    public Base findById(int id);
    public void delete(int id);
    public void add(Base base);
    public Base findByIdCard(String idCard);
    public void update(Base base);
    public List<Base> findAll(int page, int size);
}
