package com.train.epidemicds.service;

import com.train.epidemicds.entity.Source;

import java.util.List;

/**
 * author:刘哲楷
 * date:2022/10/14
 */
public interface SourceService {
    //根据医院id寻找物资
    public List<Source> findByHospitalId(int hospitalId);
    //删除物资
    public void deleteById(int id);
    //更新物资
    public void update(Source source);
    //添加物资
    public void addSource(Source source);
}
