package com.train.epidemicds.service;

import com.train.epidemicds.entity.Dead;

import java.util.List;
/**
 * author:刘炫
 * date:10/14
 */
public interface DeadService {
    void add(Dead dead);
    List<Dead> findAll(int page, int size);
    Dead findById(int baseId);
    int number();
    List<Dead> findByName(String name);
}
